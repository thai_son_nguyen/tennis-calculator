# frozen_string_literal: true

require_relative 'spec_helper'
require 'models/match_collection'
require 'models/match_recorder'
require 'commands/parser'
require 'queries/match_score'

RSpec.describe 'Integration' do
  let(:match_collection) { Models::MatchCollection.new }
  let(:match_recorder) { Models::MatchRecorder.new(match_collection: match_collection) }
  let(:data_file) { File.join(File.dirname(__FILE__), 'full_tournament.txt') }

  before do
    File.open(data_file).each do |line|
      Commands::Parser.parse(line.strip).execute(match_recorder)
    end
  end

  describe 'match score query' do
    subject(:execute_query) { query.execute(match_collection, output) }

    let(:query) { Queries::MatchScore.try_parse(query_string) }
    let(:query_string) { 'Score Match 01' }
    let(:output) { ->(s) {} }

    before do
      allow(output).to receive(:call)
    end

    it 'produces player info' do
      expect(output).to receive(:call).with('Person A defeated Person B')
      execute_query
    end

    it 'produces score info' do
      expect(output).to receive(:call).with('2 sets to 0')
      execute_query
    end
  end

  describe 'player game stats' do
    subject(:execute_query) { query.execute(match_collection, output) }

    let(:query) { Queries::PlayerGameStats.try_parse(query_string) }
    let(:query_string) { 'Games Player Person A' }
    let(:output) { ->(s) {} }

    before do
      allow(output).to receive(:call)
    end

    it 'produces game stats of the player' do
      expect(output).to receive(:call).with('23 17')
      execute_query
    end
  end
end
