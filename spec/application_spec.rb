# frozen_string_literal: true

require_relative 'spec_helper'
require 'application'

RSpec.describe Application do
  describe '#run' do
    subject(:run) { described_class.run }

    let(:data_file) { File.join(File.dirname(__FILE__), 'full_tournament.txt') }
    let(:exit_query) { 'EXIT' }

    before do
      allow(ARGV).to receive(:[]).with(0).and_return(data_file)
    end

    describe 'match query' do
      let(:query) { 'Score Match 02' }
      let(:gets_result) { double }

      before do
        allow(gets_result).to receive(:chomp).and_return(
          query, exit_query
        )
        allow(STDOUT).to receive(:puts)
        allow(described_class).to receive(:print)
        allow(described_class).to receive(:gets).and_return(gets_result)
      end

      it 'produces correct result' do
        expect(STDOUT).to receive(:puts).with('Person C defeated Person A')
        run
      end
    end

    describe 'player game query' do
      let(:query) { 'Games Player Person C' }
      let(:gets_result) { double }

      before do
        allow(gets_result).to receive(:chomp).and_return(
          query, exit_query
        )
        allow(STDOUT).to receive(:puts)
        allow(described_class).to receive(:print)
        allow(described_class).to receive(:gets).and_return(gets_result)
      end

      it 'produces correct result' do
        expect(STDOUT).to receive(:puts).with('17 11')
        run
      end
    end
  end
end
