# frozen_string_literal: true

require_relative '../spec_helper'
require 'models/match_recorder'

RSpec.describe Models::MatchRecorder do
  describe '#switch_to_match' do
    subject(:switch_to_match) { match_recorder.switch_to_match(match_id) }

    let(:match_recorder) do
      described_class.new(match_collection: match_collection,
                          match_scoring_strategy: match_scoring_strategy,
                          set_scoring_strategy: set_scoring_strategy,
                          game_scoring_strategy: game_scoring_strategy)
    end
    let(:match_collection) { instance_double(Models::MatchCollection) }
    let(:match_scoring_strategy) { class_double(Strategies::StandardMatchScoringStrategy) }
    let(:set_scoring_strategy) { class_double(Strategies::StandardSetScoringStrategy) }
    let(:game_scoring_strategy) { class_double(Strategies::StandardGameScoringStrategy) }
    let(:match_id) { '01' }

    context 'when match does NOT exist' do
      let(:new_match) { instance_double(Models::Match) }

      before do
        allow(match_collection).to receive(:add)
        allow(match_collection).to receive(:get_match).with(match_id).and_return(nil)
        allow(Models::Match).to receive(:new).with(id: match_id, scoring_strategy: match_scoring_strategy,
                                                   set_scoring_strategy: set_scoring_strategy,
                                                   game_scoring_strategy: game_scoring_strategy).and_return(new_match)
      end

      it 'creates new match' do
        expect(match_collection).to receive(:add).with(new_match)
        switch_to_match
      end

      it 'sets recording match to new match' do
        switch_to_match
        expect(match_recorder.recording_match).to be new_match
      end
    end

    context 'when match already exists' do
      let(:match) { instance_double(Models::Match) }

      before do
        allow(match_collection).to receive(:add)
        allow(match_collection).to receive(:get_match).with(match_id).and_return(match)
      end

      it 'does NOT create new match' do
        expect(match_collection).not_to receive(:add)
        switch_to_match
      end

      it 'sets recording match to match' do
        switch_to_match
        expect(match_recorder.recording_match).to be match
      end
    end
  end
end
