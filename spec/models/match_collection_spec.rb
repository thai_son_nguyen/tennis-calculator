# frozen_string_literal: true

require_relative '../spec_helper'
require 'models/match_collection'
require 'models/match'

RSpec.describe Models::MatchCollection do
  describe '#initialize' do
    subject(:match_collection) { described_class.new }

    it 'has no match' do
      expect(match_collection.all).to be_empty
    end
  end

  describe '#add' do
    subject(:add) { match_collection.add(match) }

    let(:match_collection) { described_class.new }
    let(:match) { instance_double(Models::Match) }
    let(:id) { '01' }

    before do
      allow(match).to receive(:id).and_return(id)
    end

    it 'add new match to collection' do
      add
      expect(match_collection.get_match(id)).to be match
    end
  end
end
