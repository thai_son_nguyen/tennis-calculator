# frozen_string_literal: true

require_relative '../spec_helper'
require 'models/game'

RSpec.describe Models::Game do
  describe '#initialize' do
    subject(:game) { described_class.new }

    it 'set player 1 score to 0' do
      expect(game.get_score_of(Player::ONE)).to eq 0
    end

    it 'set player 2 score to 0' do
      expect(game.get_score_of(Player::TWO)).to eq 0
    end

    it 'has no winner' do
      expect(game.winner).to be_nil
    end

    it 'is in progress' do
      expect(game.in_progress?).to be true
    end
  end

  describe '#add_score_for_player' do
    subject(:add_score) { game.add_score_for(player) }

    let(:game) { described_class.new(scoring_strategy: scoring_strategy) }
    let(:scoring_strategy) { class_double(Strategies::StandardGameScoringStrategy) }
    let(:player) { Player::ONE }
    let!(:original_score) { game.get_score_of(player) }

    before do
      allow(scoring_strategy).to receive(:decide_winner).and_return(nil)
    end

    context 'when add score for player 1' do
      let(:expected_score) { original_score + 1 }

      it 'add 1 point to player 1' do
        add_score
        expect(game.get_score_of(player)).to eq expected_score
      end
    end

    describe '#calculate winning player' do
      context 'when there is no winner decided' do
        it 'does NOT set winner' do
          add_score
          expect(game.winner).to be_nil
        end
      end

      context 'when there is winner decided' do
        before do
          allow(scoring_strategy).to receive(:decide_winner).and_return(Player::TWO)
        end

        it 'sets winner' do
          add_score
          expect(game.winner).to eq Player::TWO
        end

        it 'is NOT in progress' do
          add_score
          expect(game.in_progress?).to be false
        end
      end
    end

    context 'when there is winner decided' do
      before do
        allow(scoring_strategy).to receive(:decide_winner).and_return(Player::TWO)
      end

      it 'does NOT allow to add score' do
        game.add_score_for(player)
        expect { add_score }.to raise_error(InvalidOperationException, 'game has ended')
      end
    end
  end
end
