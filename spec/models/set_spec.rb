# frozen_string_literal: true

require_relative '../spec_helper'
require 'models/set'
require 'models/game'

RSpec.describe Models::Set do
  describe '#initialize' do
    subject(:set) { described_class.new }

    it 'set player 1 score to 0' do
      expect(set.get_score_of(Player::ONE)).to eq 0
    end

    it 'set player 2 score to 0' do
      expect(set.get_score_of(Player::TWO)).to eq 0
    end

    it 'has no winner' do
      expect(set.winner).to be_nil
    end

    it 'has no game' do
      expect(set.games.length).to eq 0
    end

    it 'is in progress' do
      expect(set.in_progress?).to be true
    end
  end

  describe '#add_score_for' do
    subject(:add_score_for) { set.add_score_for(player) }

    let(:scoring_strategy) { class_double(Strategies::StandardSetScoringStrategy) }
    let(:game_scoring_strategy) { class_double(Strategies::StandardGameScoringStrategy) }
    let(:player) { Player::ONE }
    let(:set) { described_class.new(scoring_strategy: scoring_strategy, game_scoring_strategy: game_scoring_strategy) }
    let(:new_game) { instance_double(Models::Game) }

    before do
      allow(Models::Game).to receive(:new).with(scoring_strategy: game_scoring_strategy).and_return(new_game)
      allow(new_game).to receive(:in_progress?).and_return(true)
      allow(new_game).to receive(:add_score_for)
    end

    it 'creates new game to add score' do
      add_score_for
      expect(set.games).to include new_game
    end

    it 'adds score to game' do
      expect(new_game).to receive(:add_score_for).with(player)
      add_score_for
    end

    context 'when game has ended' do
      let!(:previous_winner_score) { set.get_score_of(player) }
      let(:new_winner_score) { previous_winner_score + 1 }

      before do
        allow(new_game).to receive(:in_progress?).and_return(false)
        allow(new_game).to receive(:winner).and_return(player)
        allow(scoring_strategy).to receive(:decide_winner).and_return(player)
      end

      it 'updates set score for winner' do
        add_score_for
        expect(set.get_score_of(player)).to eq new_winner_score
      end

      it 'checks whether winner is decided' do
        expect(scoring_strategy).to receive(:decide_winner)
        add_score_for
      end

      context 'when winner is decided' do
        it 'sets winner' do
          add_score_for
          expect(set.winner).to eq player
        end

        it 'does NOT allow subsequent score to be added' do
          set.add_score_for(player)
          expect { add_score_for }.to raise_error(InvalidOperationException)
        end
      end
    end

    context 'when there is previous ended game' do
      let(:new_game_2) { instance_double(Models::Game) }

      before do
        allow(Models::Game).to receive(:new).with(scoring_strategy: game_scoring_strategy)
                                            .and_return(new_game, new_game_2)
        allow(new_game).to receive(:in_progress?).and_return(false)
        allow(new_game).to receive(:winner).and_return(player)
        allow(scoring_strategy).to receive(:decide_winner).and_return(nil)
        set.add_score_for(player)
        allow(new_game_2).to receive(:in_progress?).and_return(true)
        allow(new_game_2).to receive(:add_score_for)
      end

      it 'creates new game to add score' do
        set.add_score_for(player)
        add_score_for
        expect(set.games).to include new_game_2
      end
    end
  end
end
