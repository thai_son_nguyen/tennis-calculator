# frozen_string_literal: true

require_relative '../spec_helper'
require 'models/match'

RSpec.describe Models::Match do
  describe '#initialize' do
    subject(:match) { described_class.new(id: id) }

    let(:id) { '01' }

    it 'sets match id to given id' do
      expect(match.id).to eq id
    end

    it 'sets match player 1 score to 0' do
      expect(match.get_score_of(Player::ONE)).to eq 0
    end

    it 'sets match player 2 score to 0' do
      expect(match.get_score_of(Player::TWO)).to eq 0
    end

    it 'sets player 1 name to empty' do
      expect(match.get_name_of_player(Player::ONE)).to eq ''
    end

    it 'sets player 2 name to empty' do
      expect(match.get_name_of_player(Player::TWO)).to eq ''
    end

    it 'has no winner' do
      expect(match.winner).to be_nil
    end

    it 'has no set' do
      expect(match.sets).to be_empty
    end

    it 'is in progress' do
      expect(match.in_progress?).to be true
    end
  end

  describe '#set_name_of_player' do
    subject(:set_name_of_player) { match.set_name_of_player(player, name) }

    let(:match) { described_class.new(id: '01') }
    let(:player) { Player::ONE }
    let(:name) { 'Person A' }

    it 'updates player name' do
      set_name_of_player
      expect(match.get_name_of_player(player)).to eq name
    end
  end

  describe '#add_score_for' do
    subject(:add_score_for) { match.add_score_for(player) }

    let(:match) do
      described_class.new(id: id, scoring_strategy: scoring_strategy,
                          set_scoring_strategy: set_scoring_strategy, game_scoring_strategy: game_scoring_strategy)
    end
    let(:id) { '01' }
    let(:scoring_strategy) { class_double(Strategies::StandardMatchScoringStrategy) }
    let(:set_scoring_strategy) { class_double(Strategies::StandardSetScoringStrategy) }
    let(:game_scoring_strategy) { class_double(Strategies::StandardGameScoringStrategy) }
    let(:player) { Player::ONE }
    let(:new_set) { instance_double(Models::Set) }

    before do
      allow(Models::Set).to receive(:new).with(scoring_strategy: set_scoring_strategy,
                                               game_scoring_strategy: game_scoring_strategy)
                                         .and_return(new_set)
      allow(new_set).to receive(:in_progress?).and_return(true)
      allow(new_set).to receive(:add_score_for)
    end

    it 'creates new set to add score' do
      add_score_for
      expect(match.sets).to include new_set
    end

    it 'adds score to set' do
      expect(new_set).to receive(:add_score_for).with(player)
      add_score_for
    end

    context 'when set has ended' do
      let!(:previous_winner_score) { match.get_score_of(player) }
      let(:new_winner_score) { previous_winner_score + 1 }

      before do
        allow(new_set).to receive(:in_progress?).and_return(false)
        allow(new_set).to receive(:winner).and_return(player)
        allow(scoring_strategy).to receive(:decide_winner).and_return(player)
      end

      it 'updates match score for winner' do
        add_score_for
        expect(match.get_score_of(player)).to eq new_winner_score
      end

      it 'checks whether winner is decided' do
        expect(scoring_strategy).to receive(:decide_winner)
        add_score_for
      end

      context 'when winner is decided' do
        it 'sets winner' do
          add_score_for
          expect(match.winner).to eq player
        end

        it 'does NOT allow subsequent score to be added' do
          match.add_score_for(player)
          expect { add_score_for }.to raise_error(InvalidOperationException)
        end
      end
    end

    context 'when there is previous ended set' do
      let(:new_set_2) { instance_double(Models::Set) }

      before do
        allow(Models::Set).to receive(:new).with(scoring_strategy: set_scoring_strategy,
                                                 game_scoring_strategy: game_scoring_strategy)
                                           .and_return(new_set, new_set_2)
        allow(new_set).to receive(:in_progress?).and_return(false)
        allow(new_set).to receive(:winner).and_return(player)
        allow(scoring_strategy).to receive(:decide_winner).and_return(nil)
        match.add_score_for(player)
        allow(new_set_2).to receive(:in_progress?).and_return(true)
        allow(new_set_2).to receive(:add_score_for)
      end

      it 'creates new set to add score' do
        match.add_score_for(player)
        add_score_for
        expect(match.sets).to include new_set_2
      end
    end
  end
end
