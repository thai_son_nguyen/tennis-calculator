# frozen_string_literal: true

require_relative '../spec_helper'
require 'commands/set_player_names'
require 'models/match_recorder'

RSpec.describe Commands::SetPlayerNames do
  describe '#try_parse' do
    subject(:try_parse) { described_class.try_parse(command_string) }

    let(:command_string) { "#{player_1_name} vs #{player_2_name}" }
    let(:player_1_name) { 'Person A' }
    let(:player_2_name) { 'Person B' }
    let(:expected_command) { instance_double(described_class) }

    before do
      allow(described_class).to receive(:new).with(player_1_name, player_2_name).and_return(expected_command)
    end

    it 'creates a switch game command' do
      expect(try_parse).to be expected_command
    end

    context 'when command is not valid' do
      let(:command_string) { "#{player_1_name} v s #{player_2_name}" }

      it 'returns nil' do
        expect(try_parse).to be_nil
      end
    end
  end

  describe '#initialize' do
    subject(:set_player_names) { described_class.new(player_1_name, player_2_name) }

    let(:player_1_name) { 'Person A' }
    let(:player_2_name) { 'Person B' }

    it 'sets name of player 1 correctly' do
      expect(set_player_names.player_1_name).to eq player_1_name
    end

    it 'sets name of player 2 correctly' do
      expect(set_player_names.player_2_name).to eq player_2_name
    end
  end

  describe '#execute' do
    subject(:execute) { set_player_names.execute(recorder) }

    let(:recorder) { instance_double(Models::MatchRecorder) }
    let(:match) { instance_double(Models::Match) }
    let(:set_player_names) { described_class.new(player_1_name, player_2_name) }
    let(:player_1_name) { 'Person A' }
    let(:player_2_name) { 'Person B' }

    before do
      allow(recorder).to receive(:recording_match).and_return(match)
      allow(match).to receive(:set_name_of_player)
    end

    it 'sets name of player 1' do
      expect(match).to receive(:set_name_of_player).with(Player::ONE, player_1_name)
      execute
    end

    it 'sets name of player 2' do
      expect(match).to receive(:set_name_of_player).with(Player::TWO, player_2_name)
      execute
    end
  end
end
