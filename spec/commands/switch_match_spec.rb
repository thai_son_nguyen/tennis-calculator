# frozen_string_literal: true

require_relative '../spec_helper'
require 'commands/switch_match'
require 'models/match_recorder'

RSpec.describe Commands::SwitchMatch do
  describe '#try_parse' do
    subject(:try_parse) { described_class.try_parse(command_string) }

    let(:command_string) { "Match: #{match_id}" }
    let(:match_id) { '01' }
    let(:expected_command) { instance_double(described_class) }

    before do
      allow(described_class).to receive(:new).with(match_id).and_return(expected_command)
    end

    it 'creates a switch game command' do
      expect(try_parse).to be expected_command
    end

    context 'when command does NOT start with Match:' do
      let(:command_string) { "Matchs: #{match_id}" }

      it 'returns nil' do
        expect(try_parse).to be_nil
      end
    end
  end

  describe '#initialize' do
    subject(:switch_match_command) { described_class.new(match_id) }

    let(:match_id) { '01' }

    it 'sets match id correctly' do
      expect(switch_match_command.match_id).to eq match_id
    end
  end

  describe '#execute' do
    subject(:execute) { switch_match_command.execute(recorder) }

    let(:recorder) { instance_double(Models::MatchRecorder) }
    let(:switch_match_command) { described_class.new(match_id) }
    let(:match_id) { '01' }

    it 'calls recorder to switch match' do
      expect(recorder).to receive(:switch_to_match).with(match_id)
      execute
    end
  end
end
