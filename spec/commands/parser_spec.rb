# frozen_string_literal: true

require_relative '../spec_helper'
require 'commands/parser'

RSpec.describe Commands::Parser do
  describe '#parse' do
    subject(:parse) { described_class.parse(command_string) }

    let(:command_string) { 'aa' }
    let(:add_score) { instance_double(Commands::AddScore) }
    let(:switch_match) { instance_double(Commands::SwitchMatch) }
    let(:set_player_names) { instance_double(Commands::SetPlayerNames) }

    before do
      allow(Commands::AddScore).to receive(:try_parse).with(command_string).and_return(nil)
      allow(Commands::SwitchMatch).to receive(:try_parse).with(command_string).and_return(nil)
      allow(Commands::SetPlayerNames).to receive(:try_parse).with(command_string).and_return(nil)
    end

    context 'when command is valid add score command' do
      before do
        allow(Commands::AddScore).to receive(:try_parse).with(command_string).and_return(add_score)
      end

      it 'returns the add score command' do
        expect(parse).to be add_score
      end
    end

    context 'when command is valid switch match command' do
      before do
        allow(Commands::SwitchMatch).to receive(:try_parse).with(command_string).and_return(switch_match)
      end

      it 'returns the switch match command' do
        expect(parse).to be switch_match
      end
    end

    context 'when command is valid set player names command' do
      before do
        allow(Commands::SetPlayerNames).to receive(:try_parse).with(command_string).and_return(set_player_names)
      end

      it 'returns the set player names command' do
        expect(parse).to be set_player_names
      end
    end

    context 'when command is not a valid one' do
      let(:null_command) { instance_double(Commands::Null) }

      before do
        allow(Commands::Null).to receive(:new).with(command_string).and_return(null_command)
      end

      it 'returns null command' do
        expect(parse).to be null_command
      end
    end

    context 'when there is exception parsing command' do
      let(:null_command) { instance_double(Commands::Null) }

      before do
        allow(Commands::SetPlayerNames).to receive(:try_parse).and_raise(StandardError)
        allow(Commands::Null).to receive(:new).with(command_string).and_return(null_command)
      end

      it 'returns null command' do
        expect(parse).to be null_command
      end
    end
  end
end
