# frozen_string_literal: true

require_relative '../spec_helper'
require 'commands/add_score'
require 'models/match_recorder'

RSpec.describe Commands::AddScore do
  describe '#try_parse' do
    subject(:try_parse) { described_class.try_parse(command_string) }

    let(:command_string) { score_value.to_s }
    let(:score_value) { 0 }
    let(:expected_command) { instance_double(described_class) }

    before do
      allow(described_class).to receive(:new).with(score_value).and_return(expected_command)
    end

    it 'creates a switch game command' do
      expect(try_parse).to be expected_command
    end

    context 'when command is not valid' do
      let(:command_string) { '2' }

      it 'returns nil' do
        expect(try_parse).to be_nil
      end
    end
  end

  describe '#initialize' do
    subject(:add_score) { described_class.new(score_value) }

    let(:score_value) { 0 }

    it 'sets score value correctly' do
      expect(add_score.score_value).to eq score_value
    end
  end

  describe '#execute' do
    subject(:execute) { add_score.execute(recorder) }

    let(:recorder) { instance_double(Models::MatchRecorder) }
    let(:match) { instance_double(Models::Match) }
    let(:add_score) { described_class.new(score_value) }
    let(:score_value) { 5 }

    before do
      allow(recorder).to receive(:recording_match).and_return(match)
      allow(match).to receive(:add_score_for)
    end

    context 'when score value is 0' do
      let(:score_value) { 0 }

      it 'adds score for player 1' do
        expect(match).to receive(:add_score_for).with(Player::ONE)
        execute
      end
    end

    context 'when score value is 1' do
      let(:score_value) { 1 }

      it 'adds score for player 2' do
        expect(match).to receive(:add_score_for).with(Player::TWO)
        execute
      end
    end

    context 'when score value is NOT 0 or 1' do
      let(:score_value) { 2 }

      it 'does NOT add score' do
        expect(match).not_to receive(:add_score_for)
        execute
      end
    end
  end
end
