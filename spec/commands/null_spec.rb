# frozen_string_literal: true

require_relative '../spec_helper'
require 'commands/null'

RSpec.describe Commands::Null do
  describe '#initialize' do
    subject(:null_command) { described_class.new(command_string) }

    let(:command_string) { 'abc' }

    it 'sets command string correctly' do
      expect(null_command.command_string).to eq command_string
    end
  end

  describe '#execute' do
    subject(:execute) { null_command.execute(recorder) }

    let(:recorder) { instance_double(Models::MatchRecorder) }
    let(:null_command) { described_class.new('abc') }

    it 'does NOT do anything with recorder' do
      expect(recorder).not_to receive(:switch_to_match)
      execute
    end

    it 'does NOT do anything with match' do
      expect(recorder).not_to receive(:recording_match)
      execute
    end
  end
end
