# frozen_string_literal: true

require_relative '../spec_helper'
require 'strategies/standard_game_scoring_strategy'

RSpec.describe Strategies::StandardGameScoringStrategy do
  describe '#decide_winner' do
    subject(:decide_winner) { described_class.decide_winner(player_1_score, player_2_score) }

    let(:player_1_score) { 3 }
    let(:player_2_score) { 0 }

    context 'when player 1 score is less than 4' do
      let(:player_1_score) { 3 }

      context 'when player 1 is leading by 2 points' do
        let(:player_2_score) { player_1_score - 2 }

        it 'there is no winner' do
          expect(decide_winner).to be_nil
        end
      end
    end

    context 'when player 1 score is 4' do
      let(:player_1_score) { 4 }

      context 'when player 1 is leading by 2 points' do
        let(:player_2_score) { player_1_score - 2 }

        it 'player 1 is the winner' do
          expect(decide_winner).to eq Player::ONE
        end
      end

      context 'when player 1 is leading by 1 points' do
        let(:player_2_score) { player_1_score - 1 }

        it 'there is no winner' do
          expect(decide_winner).to be_nil
        end
      end
    end

    context 'when player 1 score is more than 4' do
      let(:player_1_score) { 5 }

      context 'when player 1 is leading by 2 points' do
        let(:player_2_score) { player_1_score - 2 }

        it 'player 1 is the winner' do
          expect(decide_winner).to eq Player::ONE
        end
      end

      context 'when player 1 is leading by 1 points' do
        let(:player_2_score) { player_1_score - 1 }

        it 'there is no winner' do
          expect(decide_winner).to be_nil
        end
      end
    end

    context 'when player 2 score is less than 4' do
      let(:player_2_score) { 3 }

      context 'when player 2 is leading by 2 points' do
        let(:player_1_score) { player_2_score - 2 }

        it 'there is no winner' do
          expect(decide_winner).to be_nil
        end
      end
    end

    context 'when player 2 score is 4' do
      let(:player_2_score) { 4 }

      context 'when player 1 is leading by 2 points' do
        let(:player_1_score) { player_2_score - 2 }

        it 'player 2 is the winner' do
          expect(decide_winner).to eq Player::TWO
        end
      end

      context 'when player 2 is leading by 1 points' do
        let(:player_1_score) { player_2_score - 1 }

        it 'there is no winner' do
          expect(decide_winner).to be_nil
        end
      end
    end

    context 'when player 2 score is more than 4' do
      let(:player_2_score) { 5 }

      context 'when player 2 is leading by 2 points' do
        let(:player_1_score) { player_2_score - 2 }

        it 'player 2 is the winner' do
          expect(decide_winner).to eq Player::TWO
        end
      end

      context 'when player 2 is leading by 1 points' do
        let(:player_1_score) { player_2_score - 1 }

        it 'there is no winner' do
          expect(decide_winner).to be_nil
        end
      end
    end
  end
end
