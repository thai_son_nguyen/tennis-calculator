# frozen_string_literal: true

require_relative '../spec_helper'
require 'strategies/standard_set_scoring_strategy'

RSpec.describe Strategies::StandardSetScoringStrategy do
  describe '#decide_winner' do
    subject(:decide_winner) { described_class.decide_winner(player_1_score, player_2_score) }

    let(:player_1_score) { 3 }
    let(:player_2_score) { 0 }

    context 'when player 1 is leading' do
      let(:player_2_score) { player_1_score - 1 }

      context 'when player 1 score is less than 6' do
        let(:player_1_score) { 5 }

        it 'there is no winner' do
          expect(decide_winner).to be_nil
        end
      end

      context 'when player 1 score is 6' do
        let(:player_1_score) { 6 }

        it 'player 1 is the winner' do
          expect(decide_winner).to eq Player::ONE
        end
      end
    end

    context 'when player 2 is leading' do
      let(:player_1_score) { player_2_score - 1 }

      context 'when player 2 score is less than 6' do
        let(:player_2_score) { 5 }

        it 'there is no winner' do
          expect(decide_winner).to be_nil
        end
      end

      context 'when player 2 score is 6' do
        let(:player_2_score) { 6 }

        it 'player 2 is the winner' do
          expect(decide_winner).to eq Player::TWO
        end
      end
    end
  end
end
