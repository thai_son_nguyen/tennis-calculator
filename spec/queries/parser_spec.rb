# frozen_string_literal: true

require_relative '../spec_helper'
require 'queries/parser'

RSpec.describe Queries::Parser do
  describe '#parse' do
    subject(:parse) { described_class.parse(query_string) }

    let(:query_string) { 'aa' }
    let(:match_score) { instance_double(Queries::MatchScore) }
    let(:player_game_stats) { instance_double(Queries::PlayerGameStats) }

    before do
      allow(Queries::MatchScore).to receive(:try_parse).with(query_string).and_return(nil)
      allow(Queries::PlayerGameStats).to receive(:try_parse).with(query_string).and_return(nil)
    end

    context 'when query is valid match score query' do
      before do
        allow(Queries::MatchScore).to receive(:try_parse).with(query_string).and_return(match_score)
      end

      it 'returns the match score query' do
        expect(parse).to be match_score
      end
    end

    context 'when query is valid player game stats query' do
      before do
        allow(Queries::PlayerGameStats).to receive(:try_parse).with(query_string).and_return(player_game_stats)
      end

      it 'returns the player game stats query' do
        expect(parse).to be player_game_stats
      end
    end

    context 'when query is not a valid one' do
      let(:null_query) { instance_double(Queries::Null) }

      before do
        allow(Queries::Null).to receive(:new).with(query_string).and_return(null_query)
      end

      it 'returns null query' do
        expect(parse).to be null_query
      end
    end

    context 'when there is exception parsing query' do
      let(:null_query) { instance_double(Queries::Null) }

      before do
        allow(Queries::PlayerGameStats).to receive(:try_parse).and_raise(StandardError)
        allow(Queries::Null).to receive(:new).with(query_string).and_return(null_query)
      end

      it 'returns null query' do
        expect(parse).to be null_query
      end
    end
  end
end
