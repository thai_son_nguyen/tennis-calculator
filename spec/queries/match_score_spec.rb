# frozen_string_literal: true

require_relative '../spec_helper'
require 'queries/match_score'
require 'models/match_collection'

RSpec.describe Queries::MatchScore do
  describe '#try_parse' do
    subject(:try_parse) { described_class.try_parse(query_string) }

    let(:query_string) { "Score Match #{match_id}" }
    let(:match_id) { '01' }
    let(:expected_query) { instance_double(described_class) }

    before do
      allow(described_class).to receive(:new).with(match_id).and_return(expected_query)
    end

    it 'creates a match score query' do
      expect(try_parse).to be expected_query
    end

    context 'when query string is not valid' do
      let(:query_string) { "Score Match: #{match_id}" }

      it 'returns nil' do
        expect(try_parse).to be_nil
      end
    end
  end

  describe '#initialize' do
    subject(:match_score_query) { described_class.new(match_id) }

    let(:match_id) { '01' }

    it 'sets match id correctly' do
      expect(match_score_query.match_id).to eq match_id
    end
  end

  describe '#execute' do
    subject(:execute) { match_score_query.execute(match_collection, output_func) }

    let(:output_func) { ->(s) {} }
    let(:match_collection) { instance_double(Models::MatchCollection) }
    let(:match_score_query) { described_class.new(match_id) }
    let(:match_id) { '01' }
    let(:match) { instance_double(Models::Match) }

    before do
      allow(output_func).to receive(:call)
      allow(match_collection).to receive(:get_match).with(match_id).and_return(match)
      allow(match).to receive(:winner).and_return(winner)
      allow(match).to receive(:get_name_of_player).with(winner).and_return(winner_name)
      allow(match).to receive(:get_name_of_player).with(loser).and_return(loser_name)
      allow(match).to receive(:get_score_of).with(winner).and_return(winner_score)
      allow(match).to receive(:get_score_of).with(loser).and_return(loser_score)
    end

    context 'when player 1 is winning' do
      let(:winner) { Player::TWO }
      let(:loser) { Player::ONE }
      let(:winner_name) { 'Person B' }
      let(:loser_name) { 'Person A' }
      let(:winner_score) { 3 }
      let(:loser_score) { 0 }

      it 'outputs player info correctly' do
        expect(output_func).to receive(:call).with("#{winner_name} defeated #{loser_name}")
        execute
      end

      it 'outputs score info correctly' do
        expect(output_func).to receive(:call).with("#{winner_score} sets to #{loser_score}")
        execute
      end
    end

    context 'when player 2 is winning' do
      let(:winner) { Player::ONE }
      let(:loser) { Player::TWO }
      let(:winner_name) { 'Person A' }
      let(:loser_name) { 'Person B' }
      let(:winner_score) { 3 }
      let(:loser_score) { 0 }

      it 'outputs player info correctly' do
        expect(output_func).to receive(:call).with("#{winner_name} defeated #{loser_name}")
        execute
      end

      it 'outputs score info correctly' do
        expect(output_func).to receive(:call).with("#{winner_score} sets to #{loser_score}")
        execute
      end
    end

    context 'when match id is not valid' do
      let(:winner) { Player::ONE }
      let(:loser) { Player::TWO }
      let(:winner_name) { 'Person A' }
      let(:loser_name) { 'Person B' }
      let(:winner_score) { 3 }
      let(:loser_score) { 0 }

      before do
        allow(output_func).to receive(:call)
        allow(match_collection).to receive(:get_match).with(match_id).and_return(nil)
      end

      it 'does NOT do anything' do
        expect(output_func).not_to receive(:call)
        execute
      end
    end
  end
end
