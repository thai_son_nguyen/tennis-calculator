# frozen_string_literal: true

require_relative '../spec_helper'
require 'queries/null'

RSpec.describe Queries::Null do
  describe '#initialize' do
    subject(:null_query) { described_class.new(query_string) }

    let(:query_string) { 'abc' }

    it 'sets query string correctly' do
      expect(null_query.query_string).to eq query_string
    end
  end

  describe '#execute' do
    subject(:execute) { null_query.execute(match_collection, output) }

    let(:match_collection) { instance_double(Models::MatchCollection) }
    let(:output) { ->(s) {} }
    let(:null_query) { described_class.new('abc') }

    it 'does NOT do anything' do
      expect(output).not_to receive(:call)
      execute
    end
  end
end
