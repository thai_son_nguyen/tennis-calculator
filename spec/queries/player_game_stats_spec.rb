# frozen_string_literal: true

require_relative '../spec_helper'
require 'queries/player_game_stats'
require 'models/match_collection'
require 'models/set'

RSpec.describe Queries::PlayerGameStats do
  describe '#try_parse' do
    subject(:try_parse) { described_class.try_parse(query_string) }

    let(:query_string) { "Games Player #{player_name}" }
    let(:player_name) { '01' }
    let(:expected_query) { instance_double(described_class) }

    before do
      allow(described_class).to receive(:new).with(player_name).and_return(expected_query)
    end

    it 'creates a query' do
      expect(try_parse).to be expected_query
    end

    context 'when query string is not valid' do
      let(:query_string) { "Games Player1 #{player_name}" }

      it 'returns nil' do
        expect(try_parse).to be_nil
      end
    end
  end

  describe '#initialize' do
    subject(:player_game_stats_query) { described_class.new(player_name) }

    let(:player_name) { '01' }

    it 'sets match id correctly' do
      expect(player_game_stats_query.player_name).to eq player_name
    end
  end

  describe '#execute' do
    subject(:execute) { player_game_stats_query.execute(match_collection, output_func) }

    let(:output_func) { ->(s) {} }
    let(:match_collection) { instance_double(Models::MatchCollection) }
    let(:player_game_stats_query) { described_class.new(player_name) }
    let(:match_1) { instance_double(Models::Match) }
    let(:match_1_set_1) { instance_double(Models::Set) }
    let(:match_1_set_1_game_1) { instance_double(Models::Game) }
    let(:match_1_set_2) { instance_double(Models::Set) }
    let(:match_1_set_2_game_1) { instance_double(Models::Game) }
    let(:match_2) { instance_double(Models::Match) }
    let(:match_2_set_1) { instance_double(Models::Set) }
    let(:match_2_set_1_game_1) { instance_double(Models::Game) }
    let(:player_name) { 'Person A' }
    let(:player_type) { Player::ONE }
    let(:not_player_type) { Player::TWO }

    before do
      allow(output_func).to receive(:call)
      allow(match_collection).to receive(:all).and_return([match_1, match_2])
      allow(match_1).to receive(:get_name_of_player).with(player_type).and_return(player_name)
      allow(match_1).to receive(:sets).and_return([match_1_set_1, match_1_set_2])
      allow(match_1_set_1).to receive(:games).and_return([match_1_set_1_game_1])
      allow(match_1_set_1_game_1).to receive(:winner).and_return(player_type)
      allow(match_1_set_2).to receive(:games).and_return([match_1_set_2_game_1])
      allow(match_1_set_2_game_1).to receive(:winner).and_return(not_player_type)
      allow(match_2).to receive(:get_name_of_player).with(player_type).and_return('')
      allow(match_2).to receive(:get_name_of_player).with(not_player_type).and_return('')
      allow(match_2).to receive(:sets).and_return([match_2_set_1])
      allow(match_2_set_1).to receive(:games).and_return([match_2_set_1_game_1])
      allow(match_2_set_1_game_1).to receive(:winner).and_return(player_type)
    end

    it 'output player game stats' do
      expect(output_func).to receive(:call).with('1 1')
      execute
    end
  end
end
