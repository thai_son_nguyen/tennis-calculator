##1. SYSTEM REQUIREMENTS
	1.1 Tested in ruby 2.6.1
    1.2 Development environment is Windows

##2. RUN INSTRUCTIONS
    2.1 Install ruby 2.6 from https://rubyinstaller.org/downloads/
	2.1 Clone the repo
    2.2 Run `bundle install`
    2.3 Copy data file to folder `lib``
    2.4 Go to folder `lib`	
    2.5 Run `ruby driver.rb <name of the file>`    

##3. DESIGN ASSUMPTIONS and CHOICES
	3.1 `Ruby` is selected as its syntax is close enough to PHP
    3.2 Console command line input is used
	3.3 Minimal error handlings
	3.4 EXIT command is added to terminate program for convenience.
	3.5 Invalid queries will be ignored
    3.6 `rspec` is selected for BDD
    3.7 All matches in data file have finished


##4. DESIGN PRINCIPLES
    4.1 The solution is a bit over-engineered to demonstrate understandings of different patterns
	4.2 Command Design Pattern
	4.3 S.O.L.I.D
	4.4 Test Driven Development.