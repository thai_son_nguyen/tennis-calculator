# frozen_string_literal: true

require_relative 'base_command'

module Commands
  class Null < BaseCommand
    attr_reader :command_string

    def initialize(command_string)
      @command_string = command_string
    end

    def execute(recorder); end
  end
end
