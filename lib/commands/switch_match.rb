# frozen_string_literal: true

require_relative 'base_command'

module Commands
  class SwitchMatch < BaseCommand
    class << self
      def try_parse(command_string)
        create_command(command_string) if PATTERN.match?(command_string)
      end

      private

      PATTERN = /^Match: (.+)\z/i.freeze

      def create_command(command_string)
        args = PATTERN.match(command_string)
        new(args[1])
      end
    end

    attr_reader :match_id

    def initialize(match_id)
      @match_id = match_id
    end

    def execute(recorder)
      recorder.switch_to_match(match_id)
    end
  end
end
