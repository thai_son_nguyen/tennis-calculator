# frozen_string_literal: true

require_relative 'base_command'
require_relative '../models/player_enum'

module Commands
  class SetPlayerNames < BaseCommand
    class << self
      def try_parse(command_string)
        create_command(command_string) if PATTERN.match?(command_string)
      end

      private

      PATTERN = /^(.+) vs (.+)\z/i.freeze

      def create_command(command_string)
        args = PATTERN.match(command_string)
        new(args[1], args[2])
      end
    end

    attr_reader :player_1_name, :player_2_name

    def initialize(player_1_name, player_2_name)
      @player_1_name = player_1_name
      @player_2_name = player_2_name
    end

    def execute(recorder)
      recorder.recording_match.set_name_of_player(Player::ONE, player_1_name)
      recorder.recording_match.set_name_of_player(Player::TWO, player_2_name)
    end
  end
end
