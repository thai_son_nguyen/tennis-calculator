# frozen_string_literal: true

require_relative 'switch_match'
require_relative 'add_score'
require_relative 'set_player_names'
require_relative 'null'

module Commands
  class Parser
    class << self
      def parse(command_string)
        try_parse(command_string) || Commands::Null.new(command_string)
      rescue StandardError
        Commands::Null.new(command_string)
      end

      private

      def try_parse(command_string)
        supported_parsers.map { |parser| parser.try_parse(command_string) }
                         .find { |command| !command.nil? }
      end

      def supported_parsers
        [Commands::SwitchMatch, Commands::SetPlayerNames, Commands::AddScore]
      end
    end
  end
end
