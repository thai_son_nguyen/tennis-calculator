# frozen_string_literal: true

require_relative 'base_command'
require_relative '../models/player_enum'

module Commands
  class AddScore < BaseCommand
    class << self
      def try_parse(command_string)
        create_command(command_string) if PATTERN.match?(command_string)
      end

      private

      PATTERN = /^([0-1])\z/i.freeze

      def create_command(command_string)
        args = PATTERN.match(command_string)
        new(args[1].to_i)
      end
    end

    attr_reader :score_value

    def initialize(score_value)
      @score_value = score_value
    end

    def execute(recorder)
      recorder.recording_match.add_score_for(Player::ONE) if score_value.zero?
      recorder.recording_match.add_score_for(Player::TWO) if score_value == 1
    end
  end
end
