# frozen_string_literal: true

require_relative '../models/player_enum'

module Strategies
  class StandardSetScoringStrategy
    class << self
      def decide_winner(player_1_score, player_2_score)
        return Player::ONE if player_1_score == 6
        return Player::TWO if player_2_score == 6
      end
    end
  end
end
