# frozen_string_literal: true

require_relative '../models/player_enum'

module Strategies
  class StandardMatchScoringStrategy
    class << self
      def decide_winner(player_1_score, player_2_score)
        return Player::ONE if player_1_score == 2
        return Player::TWO if player_2_score == 2
      end
    end
  end
end
