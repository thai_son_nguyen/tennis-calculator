# frozen_string_literal: true

require_relative 'base_query'

module Queries
  class Null < BaseQuery
    attr_reader :query_string

    def initialize(query_string)
      @query_string = query_string
    end

    def execute(match_collection, output_func) end
  end
end
