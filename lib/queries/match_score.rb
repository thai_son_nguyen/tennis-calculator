# frozen_string_literal: true

require_relative 'base_query'

module Queries
  class MatchScore < BaseQuery
    class << self
      def try_parse(query_string)
        create_query(query_string) if PATTERN.match?(query_string)
      end

      private

      PATTERN = /^Score Match (.+)\z/i.freeze

      def create_query(query_string)
        args = PATTERN.match(query_string)
        new(args[1])
      end
    end

    attr_reader :match_id

    def initialize(match_id)
      @match_id = match_id
    end

    def execute(match_collection, output_func)
      match = match_collection.get_match(@match_id)
      return if match.nil?

      winner = match.winner
      loser = loser_player_code(winner)

      output_func.call("#{match.get_name_of_player(winner)} defeated #{match.get_name_of_player(loser)}")
      output_func.call("#{match.get_score_of(winner)} sets to #{match.get_score_of(loser)}")
    end

    private

    def loser_player_code(winner)
      return Player::TWO if winner == Player::ONE

      Player::ONE
    end
  end
end
