# frozen_string_literal: true

require_relative 'match_score'
require_relative 'player_game_stats'
require_relative 'null'

module Queries
  class Parser
    class << self
      def parse(query_string)
        try_parse(query_string) || Queries::Null.new(query_string)
      rescue StandardError
        Queries::Null.new(query_string)
      end

      private

      def try_parse(query_string)
        supported_parsers.map { |parser| parser.try_parse(query_string) }
                         .find { |query| !query.nil? }
      end

      def supported_parsers
        [Queries::MatchScore, Queries::PlayerGameStats]
      end
    end
  end
end
