# frozen_string_literal: true

require_relative 'base_query'

module Queries
  class PlayerGameStats < BaseQuery
    class << self
      def try_parse(query_string)
        create_query(query_string) if PATTERN.match?(query_string)
      end

      private

      PATTERN = /^Games Player (.+)\z/i.freeze

      def create_query(query_string)
        args = PATTERN.match(query_string)
        new(args[1])
      end
    end

    attr_reader :player_name

    def initialize(player_name)
      @player_name = player_name
    end

    def execute(match_collection, output_func)
      # match = match_collection.get_match(@match_id)
      # winner = match.winner
      # loser = loser_player_code(winner)

      # output_func.call("#{match.get_name_of_player(winner)} defeated #{match.get_name_of_player(loser)}")
      # output_func.call("#{match.get_score_of(winner)} sets to #{match.get_score_of(loser)}")
      result = match_collection.all.each_with_object({ win: 0, lose: 0 }) do |m, rs|
        player_code = get_player_code(m, @player_name)
        next if player_code.nil?

        m.sets.each do |s|
          s.games.each do |g|
            stats = g.winner == player_code ? :win : :lose
            rs[stats] = rs[stats] + 1
          end
        end
      end
      output_func.call("#{result[:win]} #{result[:lose]}")
    end

    private

    def get_player_code(match, player_name)
      return Player::ONE if match.get_name_of_player(Player::ONE) == player_name
      return Player::TWO if match.get_name_of_player(Player::TWO) == player_name
    end
  end
end
