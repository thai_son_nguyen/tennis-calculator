# frozen_string_literal: true

module Queries
  class BaseQuery
    def execute(_match_collection, _output_func)
      raise NotImplementedError, 'Excute is not implemented'
    end
  end
end
