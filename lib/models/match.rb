# frozen_string_literal: true

require_relative 'player_enum'
require_relative '../strategies/standard_match_scoring_strategy'
require_relative '../strategies/standard_set_scoring_strategy'
require_relative '../strategies/standard_game_scoring_strategy'
require_relative 'invalid_operation_exception'
require_relative 'set'

module Models
  class Match
    attr_reader :winner, :id

    def initialize(id:, scoring_strategy: Strategies::StandardMatchScoringStrategy,
                   set_scoring_strategy: Strategies::StandardSetScoringStrategy,
                   game_scoring_strategy: Strategies::StandardGameScoringStrategy)
      @player_scores = { Player::ONE => 0, Player::TWO => 0 }
      @player_names = { Player::ONE => '', Player::TWO => '' }
      @winner = nil
      @id = id
      @sets = []
      @scoring_strategy = scoring_strategy
      @set_scoring_strategy = set_scoring_strategy
      @game_scoring_strategy = game_scoring_strategy
    end

    def add_score_for(player)
      guarantee_not_ended
      add_score_to_current_set(player)
    end

    def get_score_of(player)
      @player_scores[player] || 0
    end

    def set_name_of_player(player, name)
      @player_names[player] = name
    end

    def get_name_of_player(player)
      @player_names[player]
    end

    def sets
      @sets.dup
    end

    def in_progress?
      @winner.nil?
    end

    private

    def handle_score_changed
      @winner = @scoring_strategy.decide_winner(@player_scores[Player::ONE], @player_scores[Player::TWO])
    end

    def guarantee_not_ended
      raise InvalidOperationException, 'set has ended' unless in_progress?
    end

    def add_score_to_current_set(player)
      current_set = in_progress_set
      current_set.add_score_for(player)
      handle_set_ended(current_set) unless current_set.in_progress?
    end

    def in_progress_set
      if @sets.empty? || !@sets.last.in_progress?
        @sets.push(Models::Set.new(
                     scoring_strategy: @set_scoring_strategy, game_scoring_strategy: @game_scoring_strategy
                   ))
      end
      @sets.last
    end

    def handle_set_ended(set)
      @player_scores[set.winner] = @player_scores[set.winner] + 1
      handle_score_changed
    end
  end
end
