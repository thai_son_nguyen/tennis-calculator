# frozen_string_literal: true

require_relative 'match'

module Models
  class MatchCollection
    def initialize
      @matches = {}
    end

    def all
      @matches.values
    end

    def add(match)
      @matches[match.id] = match
    end

    def get_match(id)
      @matches[id]
    end
  end
end
