# frozen_string_literal: true

require_relative 'player_enum'
require_relative '../strategies/standard_game_scoring_strategy'
require_relative 'invalid_operation_exception'

module Models
  class Game
    attr_reader :winner

    def initialize(scoring_strategy: Strategies::StandardGameScoringStrategy)
      @player_scores = { Player::ONE => 0, Player::TWO => 0 }
      @winner = nil
      @scoring_strategy = scoring_strategy
    end

    def add_score_for(player)
      guarantee_not_ended
      @player_scores[player] = @player_scores[player] + 1
      handle_score_changed
    end

    def get_score_of(player)
      @player_scores[player] || 0
    end

    def in_progress?
      @winner.nil?
    end

    private

    def handle_score_changed
      @winner = @scoring_strategy.decide_winner(@player_scores[Player::ONE], @player_scores[Player::TWO])
    end

    def guarantee_not_ended
      raise InvalidOperationException, 'game has ended' unless in_progress?
    end
  end
end
