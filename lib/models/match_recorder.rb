# frozen_string_literal: true

require_relative 'match_collection'

module Models
  class MatchRecorder
    attr_reader :recording_match

    def initialize(match_collection: MatchCollection.new,
                   match_scoring_strategy: Strategies::StandardMatchScoringStrategy,
                   set_scoring_strategy: Strategies::StandardSetScoringStrategy,
                   game_scoring_strategy: Strategies::StandardGameScoringStrategy)

      @match_scoring_strategy = match_scoring_strategy
      @set_scoring_strategy = set_scoring_strategy
      @game_scoring_strategy = game_scoring_strategy
      @match_collection = match_collection
    end

    def switch_to_match(id)
      @recording_match = @match_collection.get_match(id)
      @recording_match = create_new_match(id) if recording_match.nil?
    end

    private

    def create_new_match(id)
      match = Match.new(id: id, scoring_strategy: @match_scoring_strategy,
                        set_scoring_strategy: @set_scoring_strategy,
                        game_scoring_strategy: @game_scoring_strategy)
      @match_collection.add(match)
      match
    end
  end
end
