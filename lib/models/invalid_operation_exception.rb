# frozen_string_literal: true

class InvalidOperationException < StandardError
end
