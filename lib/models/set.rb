# frozen_string_literal: true

require_relative 'player_enum'
require_relative '../strategies/standard_set_scoring_strategy'
require_relative 'invalid_operation_exception'
require_relative 'game'

module Models
  class Set
    attr_reader :winner

    def initialize(scoring_strategy: Strategies::StandardSetScoringStrategy,
                   game_scoring_strategy: Strategies::StandardGameScoringStrategy)
      @player_scores = { Player::ONE => 0, Player::TWO => 0 }
      @winner = nil
      @scoring_strategy = scoring_strategy
      @game_scoring_strategy = game_scoring_strategy
      @games = []
    end

    def add_score_for(player)
      guarantee_not_ended
      add_score_to_current_game(player)
    end

    def games
      @games.dup
    end

    def get_score_of(player)
      @player_scores[player] || 0
    end

    def in_progress?
      @winner.nil?
    end

    private

    def handle_score_changed
      @winner = @scoring_strategy.decide_winner(@player_scores[Player::ONE], @player_scores[Player::TWO])
    end

    def guarantee_not_ended
      raise InvalidOperationException, 'set has ended' unless in_progress?
    end

    def add_score_to_current_game(player)
      current_game = in_progress_game
      current_game.add_score_for(player)
      handle_game_ended(current_game) unless current_game.in_progress?
    end

    def in_progress_game
      @games.push(Game.new(scoring_strategy: @game_scoring_strategy)) if @games.empty? || !@games.last.in_progress?
      @games.last
    end

    def handle_game_ended(game)
      @player_scores[game.winner] = @player_scores[game.winner] + 1
      handle_score_changed
    end
  end
end
