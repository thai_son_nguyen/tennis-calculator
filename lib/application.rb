# frozen_string_literal: true

require_relative 'commands/parser'
require_relative 'queries/parser'
require_relative 'models/match_recorder'
require_relative 'models/match_collection'

class Application
  class << self
    def run
      match_collection = load_data(ARGV[0])
      ARGV.clear
      start(match_collection) unless match_collection.nil?
    end

    private

    def load_data(data_file)
      match_collection = Models::MatchCollection.new
      match_recorder = Models::MatchRecorder.new(match_collection: match_collection)
      File.open(data_file).each do |line|
        Commands::Parser.parse(line.strip).execute(match_recorder)
      end
      match_collection
    rescue StandardError
      puts 'Failed to load data'
    end

    def start(match_collection)
      welcome
      while (query_string = ask_for_query)
        return if /^EXIT\z/i.match?(query_string)

        execute_query(query_string, match_collection)
      end
    end

    def ask_for_query
      print '> '
      gets.chomp
    end

    def execute_query(query_string, match_collection)
      Queries::Parser.parse(query_string).execute(match_collection, ->(s) { puts s })
    end

    def welcome
      puts <<~MSG
        Welcome to tennis calculator !        
        1. `Score Match <id>` - Query scores for a particular match
        2. `Games Player <Player Name>` - Query games for player
        3.  EXIT or empty line -> To quit program.
      MSG
    end
  end
end
